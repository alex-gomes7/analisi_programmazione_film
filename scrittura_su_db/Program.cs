﻿using System;
using System.Collections.Generic;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;

namespace scrittura_su_db
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            string cs = @"server=localhost;userid=pluto;password=alex;database=analisi_programmazione_film";
            using var con = new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"mysql version:{con.ServerVersion}");
            /*using var cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES ('Interstellar','Interstellar',2014,1,169,'USA','Warner Bros')";
            try{
                cmd.ExecuteNonQuery();
                Console.WriteLine("La riga è stata inserita");
            }
            catch (Exception e){
                Console.WriteLine("Errore"+e.ToString());
            }  
            cmd.CommandText = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES ('Creed','Creed-Nato per combattere',2015,2,133,'USA','Warner Bros')";
            try{
                cmd.ExecuteNonQuery();
                Console.WriteLine("La riga è stata inserita");
            }
            catch (Exception e){
                Console.WriteLine("Errore"+e.ToString());
            }  
            cmd.CommandText = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES ('Trading Places','Una poltrona per due',1983,3,117,'USA','Disney')";
            try{
                cmd.ExecuteNonQuery();
                Console.WriteLine("La riga è stata inserita");
            }
            catch (Exception e){
                Console.WriteLine("Errore"+e.ToString());
            }  
            cmd.CommandText = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES ('Inception','Inception',2010,4,148,'USA','Warner Bros')";
            try{
                cmd.ExecuteNonQuery();
                Console.WriteLine("La riga è stata inserita");
            }
            catch (Exception e){
                Console.WriteLine("Errore"+e.ToString());
            }*/
            /*var sql = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES (@titolo_originale,@titolo_italiano,@anno_produzione,@regia_ID,@durata,@paese_produzione,@distribuzione)";
            using var cmd = new MySqlCommand(sql,con);*/
            /*cmd.Parameters.AddWithValue("@titolo_originale","Midnight Sun");
            cmd.Parameters.AddWithValue("@titolo_italiano","Il sole a mezzanotte");
            cmd.Parameters.AddWithValue("@anno_produzione","2018");
            cmd.Parameters.AddWithValue("@regia_ID","6");
            cmd.Parameters.AddWithValue("@durata","91");
            cmd.Parameters.AddWithValue("@paese_produzione","USA");
            cmd.Parameters.AddWithValue("@distribuzione","Eagle Pictures");
            cmd.Prepare();
            try {cmd.ExecuteNonQuery();
            Console.WriteLine("la riga è stata inserita");}
            catch (Exception e){Console.WriteLine("Errore"+e.ToString());}*/
            string[,] matrice = { { "Batman begins", "Batman begins", "2005", "7", "140", "USA", "Warner Bros" }, { "The dark knight", "Il cavaliere oscuro", "2008", "7", "152", "USA", "Warner Bros" }, { "The dark knight rises", "Il cavaliere oscuro il ritorno", "2012", "7", "164", "USA", "Warner Bros" }, { "Night at the museum", "Una notte al museo", "2006", "8", "108", "USA", "20th centuryfox" }, { "Night at the Museum: Battle of the Smithsonian", "Una notte al museo 2 - La fuga", "2009", "8", "105", "USA", "20th centuryfox" } };
            for (int j = 0; j < 4; j++)
            {
                var sql = "INSERT INTO movie (titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produzione,distribuzione) VALUES (@titolo_originale,@titolo_italiano,@anno_produzione,@regia_ID,@durata,@paese_produzione,@distribuzione)";
                using var cmd = new MySqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@titolo_originale", matrice[j, 0]);
                cmd.Parameters.AddWithValue("@titolo_italiano", matrice[j, 1]);
                cmd.Parameters.AddWithValue("@anno_produzione", matrice[j, 2]);
                cmd.Parameters.AddWithValue("@regia_ID", matrice[j, 3]);
                cmd.Parameters.AddWithValue("@durata", matrice[j, 4]);
                cmd.Parameters.AddWithValue("@paese_produzione", matrice[j, 5]);
                cmd.Parameters.AddWithValue("@distribuzione", matrice[j, 6]);
                cmd.Prepare();
                try
                {
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("La riga è stata inserita");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Errore " + e.ToString());
                }
            }
            TextReader reader1 = new StreamReader("attori.csv");
            var csvReader1 = new CsvReader(reader1,new System.Globalization.CultureInfo("en-US"));
            var actorsfromcsv = csvReader1.GetRecords<Actor>();
            foreach (Actor actorfromcsv in actorsfromcsv)
            {
                Console.WriteLine(" " + actorfromcsv.Name + " " + actorfromcsv.Surname);
            }


        }
        // Console.WriteLine(" " + actorfromcsv.Surname);
        class Person
        {
            private string _indirizzo;

            //  public string Name { get; set; }
            private string _name;
            public string Name
            {
                get => _name;
                set => _name = value;
            }
            public string Surname { get; set; }
            public int Year { get; set; }
            private string _fiscalcode;  // the name field
            private int _eta;  // the name field        
            public string FiscalCode    // the Name property
            {
                get => _fiscalcode;
                set => _fiscalcode = value;
            }
            public string StampaMessaggio(string messaggino)
            {
                //questa è una virtual
                return "questo è il metodo della persona" + messaggino;
            }
            protected int Age()    // the Name property
            {
                //get => _fiscalcode;
                return (2021 - (Year));
            }
            public string nomeCompleto()
            {
                return Name + "  " + Surname + " " + _fiscalcode + " " + Age();
            }
        }
        class Actor : Person
        {
            public string Role { get; set; }
            public string rolePlaying()
            {
                return "the actor's name is " + Name + "  " + Surname + " " + Role + Age();
            }
            enum ActorType
            {
                None,
                Protagonist,
                Coprotagonist,
                VoiceActor
            }
            /* public override string StampaMessaggio()
             {
                 //c'è un override
                 return "questo è il metodo dell'attore ";
             }*/
        }
    }

}


