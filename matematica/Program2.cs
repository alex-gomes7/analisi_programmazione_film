﻿using System; //Permette di usare le funzioni che sono definite in System

namespace matematica //definisce il namespace per richiamare le funzioni qui presenti in altri file
{
    class Program //classe principale
    {
        static void Main(string[] args) //metodo principale
        {
            //Definisco un nuovo oggetto rettangolo
            Rettangolo ret1= new Rettangolo(); 
            //Chiamo il metodo area presente in Rettangolo e li stampa in console
            Console.WriteLine(ret1.area(15)); 
            //chiamo il metodo area presente in Shape e li stampa in console
            Console.WriteLine("L'area del rettangolo è: "+ret1.area(15,40));
            //Faccio stampare la media dei numeri passati in input
            Console.WriteLine("La media è: "+ret1.calcolaMedia(5,6,7,8,10)); 
            //Definisco nuovo oggetto Omnia
            Omnia omnia1= new Omnia();
            //Chiamo metodo area di omnia
            omnia1.area();
            //Definisco nuovo oggetto cerchio
            Cerchio cerchio1= new Cerchio();
            //Calcola area di cerchio
            Console.WriteLine("L'area del cerchio è: "+cerchio1.area(5));
            //Calcola circonferenza del cerchio
            Console.WriteLine("La circonferenza è: "+cerchio1.perimetro(5.0));
            //Definisce nuovo oggetto Teoremi         
            Teoremi teo= new Teoremi();
            //Chiamo il metodo Pitagora
            Console.WriteLine("Teorema di pitagora: "+teo.Pitagora(15000,25000));
            //Definisce nuovo oggetto Teoremi 
            Ricorsione ric = new Ricorsione();
            //Chiamo il metodo fattoriale
            Console.WriteLine(ric.fattoriale(10));
            //Stampa conenuto della variabile contatore
            Trapezio trape1= new Trapezio();
            //Definisco un nuovo oggetto trapezio
            Console.WriteLine("L'area del trapezio è "+trape1.area(20,10,5));
            //Calcola l'area del trapezio
            Triangolo tri1= new Triangolo();
            //Definisco un nuovo oggetto triangolo
            Console.WriteLine("L'area del triangolo è "+tri1.area(20,5));
            //Calcola l'area del triangolo
            Console.WriteLine(Shape.contatore);
        } //CHIUDO MAIN
    } //CHIUDO CLASSE PRINCIPALE

    class Shape{ //APRO CLASSE SHAPE


        public static int contatore; //Definisco variabile pubblica
        public Shape(){ //Definisco costruttore di Shape
            contatore++; //incremento la variabile ogni volta  che chiamo il costruttore 
        } //chiudo costruttore

        public int X{get;set;} //Definizione proprietà X
        public int Y{get;set;}//Definizione proprietà Y

        public int width{get;set;}//Definizione proprietà width

        public int height{get;set;}//Definizione proprietà height


        //definisco una funzione perimetro con un argomento in entrata di tipo double
        public double perimetro(double a){ 
            return 0; //non ho svolto operazioni quindi torno un risultato predefinito
        }

        //intestazione funzione calcolaMedia che prende in input array di numeri
        public double calcolaMedia(params double[] array){
            double media=0; //definizione variabile
            //Scorri i finché i é minore della lunghezza dell'array e incrementa i alla fine
            for(int i=0;i<array.Length;i++)
            {
                media+=array[i]; //mi calcolo la somma di tutti gli elementi dell'array prendendoli uno a uno
            }
            return (media/array.Length); //restituisco la somma dividendola per la lunghezza dell'array
        }
        //intestazione funzione area con 2 parametri in entrata
        public double area(double pbase ,double altezza){
            return (pbase*altezza); //restituisco il prodotto tra le due variabili
        }
    } //CHIUDO CLASSE SHAPE
    class Rettangolo:Shape{ //definizione di una nuova classe che estende Shape
        //definisco la funzione area già presente dentro shape ma con un solo parametro in entrata
        public double area(double pbase){ 
            return (pbase*pbase); //restituisce il quadrato della variabile in ingresso
        }
    }
    class Omnia:Shape{
        //definisco la funzione area già presente dentro shape ma con nessun parametro in entrata cambiato il tipo restituito
        public void area(){ 
            Console.WriteLine("Inserisci almeno un parametro"); //Scrivi in console ciò che è scritto tra virgolette
        }
    }

    class Cerchio:Shape{
        const double pi=Math.PI; //definizione costante di valore uguale a quello della variabile PI presente in Math
        public double area(double raggio){
            return (raggio*raggio*pi); //formula matematica area cerchio
        }
        //usiamo il nome di perimetro per ridefinire il metodo perimetro di Shape per la classe cerchio
        public new double perimetro(double raggio){ 
            return (2*raggio*pi);//formula matematica circonferenza cerchio
        }
    }

    class Teoremi{
        //Impostiamo dei valori di default alle 2 variabili
        public double Pitagora(double pbase=6,double altezza=8){
             //restituisce radice quadrata della potenza della base + altezza*altezza
            return Math.Sqrt(Math.Pow(pbase,2) + altezza*altezza);
        }
        
    }
    class Ricorsione{

        //usiamo long per indicare un intero di lunghezza maggiore di int
        public long fattoriale(long numero){ 
            if(numero<=1){ //se numero è minore o uguale a 1 =>CASO BASE
                return 1; //restituisci 1
            } 
            else{ //altrimenti => CASO RICORSIVO
                //restuisci numero per il risultato della chiamata alla funzione (n-1) fino ad arrivare al caso base
                return numero*fattoriale(numero-1); 

            }
        }
    }
    class Trapezio:Shape{
    public double area (double bmag,double bmin, double altezza){
    return ((bmag+bmin)*altezza/2);
    }
    }
    class Triangolo:Shape{
        public new double area (double pbase,double altezza){
            return (pbase*altezza/2);
        }
    }
}


